import { Injectable } from '@angular/core';
import { IAppUser } from './AppUser';
import {HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';

@Injectable({
    providedIn:'root'
})

export class AppUserService {

    private saveUrl = 'http://localhost:8080/app_user/app_user';
    constructor(private http: HttpClient){}
    private model: IAppUser;

    saveAppUser (
        name: string, 
        userName: string, 
        password: string, 
        email: string, 
        phoneNumber:string, 
        ){
        this.http.post(this.saveUrl, 
            {   name: name,
                userName: userName,
                password: password,
                email: email,
                phoneNumber: phoneNumber,
                role: 'STUDENT'
            }
        ).subscribe(
            (data:any) => {
                console.log(data)
            }
        )
    }


    private handleError(err: HttpErrorResponse){
        let errorMessage = '';
        if(err.error instanceof ErrorEvent){
            errorMessage = `An error occured: ${err.error.message}`;
        } else{
            errorMessage = `Server returned code: ${err.status}, error message is: ${err.message}`;
        }
        console.error(errorMessage);
        return throwError(errorMessage);
    }


}