export interface ICourse{
    id: number;
    courseCode: string
    courseName: string;
    credit: number;
}