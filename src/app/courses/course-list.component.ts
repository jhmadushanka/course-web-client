import { Component, OnInit } from "@angular/core";
import { ICourse } from './course';
import { CourseService } from './course.service';

@Component({
    selector : 'pm-courses',
    templateUrl : './course-list.component.html',
    styleUrls : ['./course-list.component.css']
})

export class CourseListComponent implements OnInit{
    pageTitle:String = 'Course List';
    imageWidth:number = 50;
    imageMargin:number = 2;
    showImage:boolean = false;
    errorMessage : string;

    _listFilter:string;

    get listFilter():string{
        return this._listFilter;
    }

    set listFilter(value:string){
        this._listFilter = value;
        this.filteredCourses = this.listFilter ? this.performFilter(this.listFilter):this.courses;
    }

    filteredCourses : ICourse[];
    courses: ICourse[] = [];

    toggleImage() :void{
        this.showImage = !this.showImage;
    }

    ngOnInit() : void{
        this.courseService.getCourses().subscribe(
            courses => {
                this.courses =courses;
                this.filteredCourses = this.courses;
            },
            error => this.errorMessage = <any>error
        );
        
    }

    constructor( private courseService: CourseService){
        this.listFilter = ''
    }

    performFilter(filterBy:string) : ICourse[]{
        filterBy = filterBy.toLowerCase();
        return this.courses.filter((course:ICourse) =>
            course.courseName.toLowerCase().indexOf(filterBy) !==-1);
    }
}