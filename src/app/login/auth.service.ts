import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable(
  {providedIn: 'root'}
  )
export class AuthService {

  private authUrl = 'http://localhost:8080/user/auth';

  constructor( private http: HttpClient) { }

  getAuthenticateUser(username, password){
    return this.http.post(this.authUrl,{ 
      username, 
      password
    }).subscribe(data => {
      console.log(data," is what we got from the server")
    })
  }
}
