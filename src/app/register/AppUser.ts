export interface IAppUser{
    id: number;
    name: string
    userName: string;
    password: string;
    email: string;
    phoneNumber: string;
    role: string;
}