import { BrowserModule } from '@angular/platform-browser';
import { NgModule, Component } from '@angular/core';
import {FormsModule} from '@angular/forms'

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CourseListComponent } from './courses/course-list.component';
import { ConvertToSpacesPipe } from 'src/shared/convert-to-spaces.pipe';
import { HttpClientModule } from '@angular/common/http';
import { CourseDetailComponent } from './courses/course-detail.component';
import { WelcomeComponent } from './home/welcome.component';
import { RouterModule } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { StudentComponent } from './student/student.component';
import { RegisterComponent } from './register/register.component';
import { LogoutComponent } from './logout/logout.component';

@NgModule({
  declarations: [
    AppComponent,
    CourseListComponent,
    ConvertToSpacesPipe,
    CourseDetailComponent,
    WelcomeComponent,
    LoginComponent,
    StudentComponent,
    RegisterComponent,
    LogoutComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    RouterModule.forRoot([
      { path: 'login', component: LoginComponent },
      { path: 'logout', component: LogoutComponent},
      { path: 'register', component: RegisterComponent},
      { path: 'student', component: StudentComponent },
      { path: 'courses', component: CourseListComponent },
      { path: 'courses/:id', component: CourseDetailComponent },
      { path: 'welcome', component: WelcomeComponent },
      { path: '', redirectTo: 'welcome', pathMatch : 'full' },
      { path: '**', redirectTo: 'welcome', pathMatch : 'full' }

    ])
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
