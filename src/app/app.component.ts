import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Course Web';
  pageTitle = 'ABC Course Web';
  isAuthenticated : boolean = false;

  getIsAuthenticated(): boolean{
    return this.isAuthenticated;
  }
}
