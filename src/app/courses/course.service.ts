import { Injectable } from '@angular/core';
import { ICourse } from './course';
import {HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';

@Injectable({
    providedIn:'root'
})
export class CourseService{

    private fetchUrl = 'http://localhost:8080/course/all';
    constructor(private http: HttpClient){}

    getCourses(): Observable<ICourse[]>{
        return this.http.get<ICourse[]>(this.fetchUrl).pipe(
            tap(data => console.log('All: '+JSON.stringify(data))),
            catchError(this.handleError)
            );
    }

    private handleError(err: HttpErrorResponse){
        let errorMessage = '';
        if(err.error instanceof ErrorEvent){
            errorMessage = `An error occured: ${err.error.message}`;
        } else{
            errorMessage = `Server returned code: ${err.status}, error message is: ${err.message}`;
        }
        console.error(errorMessage);
        return throwError(errorMessage);
    }

}