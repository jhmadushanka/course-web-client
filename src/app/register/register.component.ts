import { Component, OnInit, ViewChild } from '@angular/core';
import { AppUserService } from './AppUser.service';

class User {
  constructor(
    public name: string = '',
    public userName: string = '',
    public password: string = '',
    public email: string = '',
    public phoneNumber: string = '',
    public role: string = ''
  ){}
}

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})


export class RegisterComponent implements OnInit {

  model: User = new User();
  @ViewChild('f') form: any;

  constructor(private appUserService: AppUserService) { }

  ngOnInit() {
    
  }

  saveAppUser() : void {
    console.log("Harsha");

    if (this.form.valid) {
      console.log("Form Submitted!");
      this.appUserService.saveAppUser(
        this.model.name,
        this.model.userName, 
        this.model.password, 
        this.model.email, 
        this.model.phoneNumber, 
      );
      this.form.reset();
    }
  }

}
